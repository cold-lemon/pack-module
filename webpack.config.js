/*created by 小饭桶 on 2018/11/28 */
const path = require('path')
module.exports = {
  entry: "./src/index.jsx",
  output: {
    path: path.resolve(__dirname,'dist'), filename: "main.js"
  },
  module: {
    rules: [
      {
        test: /\.(css|less|sass)$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      },
      {
        test: /\.(js|jsx)$/i,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    contentBase: './dist'
  }
};
