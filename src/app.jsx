/*created by 小饭桶 on 2018/11/29 */
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import HomePage from './page/Homepage';

import React from 'react';
class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={ HomePage } />
          <Redirect to={HomePage} />
        </Switch>
      </Router>
    );
  }
}

export default App;
