/*created by 小饭桶 on 2018/11/28 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import './index.less';

ReactDOM.render(
  <div>
    <App />
  </div>,
  document.getElementById('root')
);
