/*created by 小饭桶 on 2018/11/29 */
import React from 'react';
import './Homepage.less';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';

const { SubMenu } = Menu;
const {
  Header, Content, Footer, Sider,
} = Layout;

class Homepage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div className="home-wrap">Homepage</div>;
  }
}

export default Homepage;
